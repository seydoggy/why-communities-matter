# case studies

## Julia - Waterloo
- Julia lives in Cambridge
- started meetup in Waterloo
- met Julia at #kwdm
- became co-organizer at #kwdm
- fast friends
- intro co-ops to her
- going to Florida together

## Dan and PJ
- started TinyHippos
- bought by BlackBerry
- met at #kwdm
- started StartupDrinks Waterloo
- got funding in part due to exposure from meetups
- pitched at DemoCamp, #kwdm, #gwmm, StartupDrinks, etc...
- I attended wedding
- going to Florida together
- started bithound.io
- they help CTi co-ops with a lot of things

## Karl and Amy
- New to Canada and the tech/start-up scene
- met at #kwdm
- started a small, stable design firm
- bought by Cober, a bigger design firm.
- Cober and Canadian Tire have a lot in common
- Cober is inspired by Canadian Tire and use us as a case study for innovation

## WillPwn4Food
- met Jen Davies at #dcg
- met Eric Davies at #kwdm
- Jen and Eric hired John who I met at #dcg
- I met Blake at #dcg
- I met Ivan at #kwdm
- met Ivan again at Toronto Freelance Group and learned of startup
- Ivan, John and Blake went to a gaming hackathon at Brydons workspace
- Ivan, John and Blake started WillPwn4Food
- they approached me to work with them
- by working out of communitech I knew of CT

## Communitech
- met Glen at #kwdm
- Glen works for Communitech
- met Rob and #kwdm
- Rob now works at Communitech
- Communitech is part of CDMN.
- Canadian Tire is at Communitech
- Rob runs app factory and run a similar model to CTi with respect to  co-ops
- played a role in helping get our co-ops off the ground

# Things I wouldn’t know

- node.js
- angular
- yeoman
- testing
- backbone
- ember
- photoshop
- css transitions
- html5
- NoSQL
